<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword;

use Brightfish\OnePassword\Item\Category;
use Brightfish\OnePassword\Item\Field;
use Brightfish\OnePassword\Item\FieldFactory;
use Brightfish\OnePassword\Item\URL;

abstract class AItem {

    public readonly string $title;
    public readonly ?string $id;
    public readonly ?int $version;
    public readonly Category $category;

    /**
     * @var Section[]
     */
    public readonly array $sections;
    /**
     * @var URL[]
     */
    public readonly array $urls;
    public readonly ?Vault $vault;

    public readonly \DateTime $createdAt;
    public readonly \DateTime $updatedAt;

    private readonly \stdClass $_rawData;

    /**
     * @var Field[]
     */
    private $_fields = [];

    public function __construct(\stdClass $data) {
        $this->title = $data->title;
        $this->id = $data->id ?? NULL;
        $this->version = $data->version ?? NULL;
        $this->category = Category::TryFrom($data->category);

        if (isset($data->vault)) {
            $this->vault = Vault::FromStdClass($data->vault);
        } else {
            $this->vault = NULL;
        }
        if (isset($data->created_at)) {
            $this->createdAt = new \DateTime($data->created_at);
        }
        if (isset($data->updated_at)) {
            $this->updatedAt = new \DateTime($data->updated_at);
        }

        $sections = [];
        foreach ($data->sections ?? [] as $section) {
            $sections[] = new Section($section->id, $section->label ?? NULL);
        }
        $this->sections = $sections;

        $fieldFactory = FieldFactory::GetInstance();

        if (!empty($data->fields)) {
            foreach ($data->fields as $field) {
                if ($field !== NULL) {
                    $this->_fields[] = $fieldFactory->createField($field, $this->sections);
                }
            }
        }

        $urls = [];
        if (isset($data->urls)) {
            foreach ($data->urls as $url) {
                $urls[] = new URL($url->primary ?? FALSE, $url->href);
            }
        }
        $this->urls = $urls;

        $this->_rawData = $data;
    }

    public function getFieldById(string $id) : ?Field {
        foreach ($this->_fields as $field) {
            if ($field->id === $id) {
                return $field;
            }
        }
        return NULL;
    }

    /**
     * @param string $type
     * @return Field[]
     */
    public function getFieldsByType(string|Field\Type $type) : array {
        if (is_string($type)) {
            $type = Field\Type::TryFrom($type);
            if ($type === NULL) {
                return [];
            }
        }
        return array_filter($this->_fields, function (Field $field) use ($type) : bool {
            return $field->type === $type;
        });
    }

    /**
     * @return Field[]
     */
    public function getFields() : array {
        return $this->_fields;
    }


    /**
     * @param string $sectionId
     * @return Field[]
     */
    public function getFieldsForSection(Section $section) : array {
        return array_filter($this->_fields, function (Field $field) use ($section) {
            return $field->section === $section;
        });
    }

    public function getSectionByLabel(string $label) : ?Section {
        foreach ($this->sections as $section) {
            if ($section->label === $label) {
                return $section;
            }
        }
        return NULL;
    }

    public function getRaw() : \stdClass {
        return $this->_rawData;
    }
    

    public function getNotes() : ?string {
        return $this->getFieldById('notesPlain')?->value;
    }

    /**
     * @return URL[]
     */
    public function getURLs() : array {
        return $this->urls;
    }
}
