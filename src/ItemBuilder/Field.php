<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword\ItemBuilder;

use Brightfish\OnePassword\Item\Field\Type;
use Brightfish\OnePassword\Section;

final class Field {

    public string $label;
    public ?Type $type;
    public ?Section $section;
    public string $value;

    public static function FromReadonlyField(\Brightfish\OnePassword\Item\Field $field) : self {
        $fieldObj = new self;
        $fieldObj->label = $field->label;
        $fieldObj->type = $field->type;
        $fieldObj->section = $field->section;
        $fieldObj->value = $field->value;

        return $fieldObj;
    }
}
