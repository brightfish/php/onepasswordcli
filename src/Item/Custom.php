<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword\Item;

use Brightfish\OnePassword\AItem;

class Custom extends AItem {

    public function getUsername() : ?string {
        return $this->getFieldById('username')?->value;
    }

    public function getPassword() : string {
        return $this->getFieldById('password')?->value;
    }
}
