<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword\Item;

use Brightfish\OnePassword\Item\Field\Type;
use Brightfish\OnePassword\Section;

class Field {

    public readonly string $id;
    public readonly ?Type $type;
    public readonly ?string $label;

    public readonly ?string $value;

    public readonly ?Section $section;

    private \stdClass $_rawData;

    /**
     * @param \stdClass $data
     * @param Section[] $sections
     */
    public function __construct(\stdClass $data, array $sections) {
        $this->_rawData = $data;

        $this->id = $data->id;
        $this->type = Type::TryFrom($data->type);
        $this->label = $data->label ?? NULL;
        $this->value = $data->value ?? NULL;

        if (isset($data->section)) {
            foreach ($sections as $section) {
                if ($section->id === $data->section->id) {
                    $this->section = $section;
                }
            }
        } else {
            $this->section = NULL;
        }
    }

    public function getRaw() : \stdClass {
        return $this->_rawData;
    }

}
