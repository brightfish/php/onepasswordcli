<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword\Item\Field;

use Brightfish\OnePassword\Item\Field;
use OTPHP\TOTP;

class OTP extends Field {

    private readonly TOTP $_totp;

    public function __construct(\stdClass $data, array $sections) {
        parent::__construct($data, $sections);

        $this->_totp = TOTP::create($this->value);
    }

    public function getOTP() : string {
        return $this->_totp->now();
    }
}
