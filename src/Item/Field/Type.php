<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword\Item\Field;

enum Type : string {
    case OTP = 'OTP';
    case URL = 'URL';
    case Concealed = 'CONCEALED';
    case Date = 'DATE';
    case String = 'STRING';
}
