<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword\Item;

use Brightfish\OnePassword\AItem;

class Password extends AItem {

    public function getPassword() : string {
        return $this->getFieldById('password')->value;
    }
}
