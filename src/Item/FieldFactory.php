<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword\Item;

use Brightfish\OnePassword\Section;
use Brightfish\OnePassword\Item\Field\Type;

class FieldFactory {

    private static string $_FactoryClass = FieldFactory::class;

    private static ?FieldFactory $_Instance = NULL;

    private function __construct() {

    }

    /**
     * @param \stdClass $field
     * @param Section[] $sections
     * @return Field
     */
    public function createField(\stdClass $field, array $sections) : Field {
        return match (Type::TryFrom($field->type)) {
            Type::OTP => $this->createOTPField($field, $sections),
            default => $this->createDefaultField($field, $sections),
        };
    }

    public function createDefaultField(\stdClass $field, array $sections) : Field {
        return new Field($field, $sections);
    }

    public function createOTPField(\stdClass $field, array $sections) : Field\OTP {
        return new Field\OTP($field, $sections);
    }

    public static function SetFactoryClass(string $class) : void {
        if (!is_a($class, FieldFactory::class, TRUE)) {
            throw new \LogicException('Class ' . $class . ' should extend class ' . FieldFactory::class);
        }
        self::$_FactoryClass = $class;
    }

    public static function GetInstance() : static {
        if (self::$_Instance === NULL) {
            self::$_Instance = new self::$_FactoryClass;
        }
        return self::$_Instance;
    }
}
