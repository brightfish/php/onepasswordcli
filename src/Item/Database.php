<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword\Item;

use Brightfish\OnePassword\AItem;

class Database extends AItem {

    public function getUsername() : ?string {
        return $this->getFieldById('username')?->value;
    }

    public function getPassword() : ?string {
        return $this->getFieldById('password')?->value;
    }

    public function getDatabaseType() : ?string {
        return $this->getFieldById('database_type')?->value;
    }

    public function getHostname() : ?string {
        return $this->getFieldById('hostname')?->value;
    }

    public function getPort() : ?string {
        return $this->getFieldById('port')?->value;
    }

    public function getDatabase() : ?string {
        return $this->getFieldById('database')?->value;
    }
}
