<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword\Item;

class URL {

    public function __construct(public readonly bool $primary, public readonly string $href) {

    }

}
