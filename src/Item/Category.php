<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword\Item;

enum Category : string {
    case Login = 'LOGIN';
    case SecureNote = 'SECURE_NOTE';
    case CreditCard = 'CREDIT_CARD';
    case Identity = 'IDENTITY';
    case Password = 'PASSWORD';
    case Document = 'DOCUMENT';

    case APICredential = 'API_CREDENTIAL';
    case BankAccount = 'BANK_ACCOUNT';
    case CryptoWallet = 'CRYPTO_WALLET'; //still appears as custom through cli
    case Database = 'DATABASE';
    case DriverLicense = 'DRIVER_LICENSE';
    case EmailAccount = 'EMAIL_ACCOUNT';
    case MedicalRecord = 'MEDICAL_RECORD';
    case Membership = 'MEMBERSHIP';
    case OutdoorLicense = 'OUTDOOR_LICENSE';
    case Passport = 'PASSPORT';
    case RewardProgram = 'REWARD_PROGRAM';
    case SSHKey = 'SSH_KEY';
    case Server = 'SERVER';
    case SocialSecurityNumber = 'SOCIAL_SECURITY_NUMBER';
    case SoftwareLicense = 'SOFTWARE_LICENSE';
    case WirelessRouter = 'WIRELESS_ROUTER';

    case Custom = 'CUSTOM';
}
