<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword\Item;

use Brightfish\OnePassword\AItem;

class WirelessRouter extends AItem {

    public function getName() : ?string {
        return $this->getFieldById('name')?->value;
    }

    public function getSSID() : ?string {
        return $this->getFieldById('network_name')?->value;
    }

    public function getWirelessPassword() : ?string {
        return $this->getFieldById('wireless_password')?->value;
    }

    public function getBaseStationPassword() : ?string {
        return $this->getFieldById('password')?->value;
    }

    public function getBaseStationIP() : ?string {
        return $this->getFieldById('server')?->value;
    }

}
