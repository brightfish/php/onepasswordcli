<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword;

use Brightfish\OnePassword\Vault;
use Brightfish\OnePassword\Vault\Icon;
use Brightfish\Utility\Shell;
use Composer\Semver\VersionParser;

final class CLI {

    public const CLI_VERSION_CONSTRAINT = '^2.14.0';

    /**
     * @var null|Account[]
     */
    private ?array $_accountCache = NULL;

    private static bool $_BiometricEnabled = TRUE;

    private static ?IUserInput $_UserInput = NULL;

    private int $_maxPasswordAttempts = 10;

    private ?string $_session = NULL;

    private readonly Shell $_shell;

    public function __construct(?Shell $shell = NULL) {
        if ($shell === NULL) {
            $shell = new Shell;
        }
        $this->_shell = $shell;

        if (!$this->isBinaryInstalled()) {
            throw new \RuntimeException('Unable to execute OP CLI commands. Binary is not installed! Please follow the installation steps at https://1password.com/downloads/command-line/');
        }
        if (!$this->isCorrectVersionInstalled()) {
            throw new \RuntimeException('Unable to execute OP CLI commands. Binary is incompatible! Got version: ' . $this->getInstalledVersion() . ' Expected to match constraint: ' . self::CLI_VERSION_CONSTRAINT);
        }
    }

    public function signIn(string $account) : bool {
        if ($this->isSignedIn($account)) {
            return TRUE;
        }
        if (!$this->hasAccount($account)) {
            return FALSE;
        }

        if (self::$_BiometricEnabled) {
            return $this->_signinWithBiometrics($account);
        } elseif (self::$_UserInput !== NULL) {
            return $this->_signinWithPassword($account);
        } else {
            throw new \RuntimeException('Unable to sign in to account: ' . $account . '. No UserInput class provided and Biometrics are disabled!');
        }
    }

    public function signOut() : void {
        $this->_runOPCommand('signout --all');
    }

    private function _signinWithBiometrics(string $account) : bool {
        $result = $this->_runOPCommand($this->_getSigninCommand($account), ' ');
        if ($result['exitcode'] === 6) { //could not authenticate, biometric is not enabled.
            self::$_BiometricEnabled = FALSE;
            return $this->_signinWithPassword($account);
        } elseif ($result['exitcode'] === 0) {
            self::ClearSessionCache();
        }
        return $result['exitcode'] === 0;
    }

    private function _signinWithPassword(string $account) : bool {
        $numPasswordAttempts = 0;
        $accountObj = $this->getAccount($account);

        $cachedSession = $this->_getCachedSessionForUserUUID($accountObj->userUuid);
        if ($cachedSession) {
            if ($this->_signinWithSession($accountObj->userUuid, $cachedSession)) {
                return TRUE;
            } else {
                $this->_removeCachedSessionForUserUUID($accountObj->userUuid);
            }
        }

        do {
            $result = $this->_runOPCommand($this->_getSigninCommand($account) . ' --raw', self::$_UserInput->getPasswordForAccount($accountObj->email, $accountObj->getDomainName(), $numPasswordAttempts));
            if ($result['exitcode'] === 1) {
                if (str_contains($result['stderr'], 'Enter your six-digit authentication code') || str_contains($result['stderr'], 'MFA Type Not Supported')) {
                    throw new \RuntimeException('Multifactor authentication enabled for account. Please use the desktop application binding to authenticate!');
                } elseif (str_contains($result['stderr'], 'Unauthorized')) {
                    ++$numPasswordAttempts;
                }
            }
        } while ($result['exitcode'] !== 0 && $numPasswordAttempts < $this->_maxPasswordAttempts);

        $session = $result['stdout'];
        $this->_signinWithSession($accountObj->userUuid, $session);

        return $result['exitcode'] === 0;
    }

    private function _signinWithSession(string $userUuid, string $session) : bool {
        $this->_session = 'OP_SESSION_' . $userUuid . '=' . $session;

        $result = $this->isSignedIn();
        if ($result) {
            $this->_cacheSessionForUserUUID($userUuid, $session);
        }
        return $result;
    }

    private function _getSigninCommand(string $account) : string {
        return 'signin --account ' . escapeshellarg($account);
    }

    public function addAccount(?string $signInAddress, ?string $email) : bool {
        return $this->_addAccount($signInAddress, $email, FALSE);
    }

    public function addAccountAndSignin(?string $signInAddress, ?string $email) : bool {
        return $this->_addAccount($signInAddress, $email, TRUE);
    }

    private function _addAccount(?string $signInAddress, ?string $email, bool $signIn) : bool {
        if (self::$_UserInput === NULL) {
            throw new \RuntimeException('Unable to add account, no UserInput class provided to interact with!');
        }
        $account = self::$_UserInput->addAccount($signInAddress, $email);
        if ($account->secretKey === NULL) {
            throw new \RuntimeException('Unable to add account, no secret key provided for account: ' . $account->url);
        }
        putenv('OP_SECRET_KEY=' . $account->secretKey);

        $password = self::$_UserInput->getPasswordForAccount($account->email, $account->getDomainName(), 0);

        if ($signIn) {
            $flags = '--signin --raw';
        } else {
            $flags = '';
        }

        $result = $this->_runOPCommand('account add ' . $flags . ' --address ' . escapeshellarg($account->url) . ' --email ' . escapeshellarg($account->email), $password);

        if ($signIn && $result['exitcode'] === 0) {
            $this->_flushAccountCache();
            $registeredAccount = $this->getAccount($account->url);
            if ($registeredAccount === NULL) {
                throw new \Exception('No account for URL' . $account->url . ' could be found!');
            }
            return $this->_signinWithSession($registeredAccount->userUuid, $result['stdout']);
        }

        return $result['exitcode'] === 0;
    }

    public function hasAccount(string $account) : bool {
        return $this->getAccount($account) !== NULL;
    }

    public function getAccount(string $account) : ?Account {
        foreach ($this->getAccounts() as $accountObj) {
            if ($accountObj->shorthand === $account || $accountObj->url === $account || $accountObj->getDomainName() === $account) {
                return $accountObj;
            }
        }
        return NULL;
    }

    /**
     * @return Account[]
     */
    public function getAccounts() : array {
        if ($this->_accountCache === NULL) {
            $this->_accountCache = [];
            $output = $this->_runOPCommand('account list');
            if ($output['exitcode'] !== 0) {
                throw new \RuntimeException('Unable to retrieve accounts, received exit code: ' . $output['exitcode'] . '. stdErr: ' . $output['stderr']);
            }
            $data = json_decode($output['stdout'], FALSE, 512, JSON_THROW_ON_ERROR);

            foreach ($data as $account) {
                $this->_accountCache[] = new Account($account);
            }
        }

        return $this->_accountCache;
    }

    private function _flushAccountCache() : void {
        $this->_accountCache = NULL;
    }

    /**
     * @return \Generator<Vault>
     * @throws \JsonException
     */
    public function listVaults(?string $group = NULL, ?string $user = NULL) : \Generator {
        $cmd = 'vault list';

        if ($group !== NULL) {
            $cmd .= '--group ' . escapeshellarg($group);
        }
        if ($user !== NULL) {
            $cmd .= '--user ' . escapeshellarg($user);
        }

        $output = $this->_runOPCommand($cmd);
        if ($output['exitcode'] !== 0) {
            throw new \RuntimeException('Unable to list vaults. received exit code: ' . $output['exitcode'] . '. stdErr: ' . $output['stderr']);
        }
        $vaults = json_decode($output['stdout'], flags: JSON_THROW_ON_ERROR);

        foreach ($vaults as $vault) {
            yield Vault::FromStdClass($vault);
        }
    }

    public function createVault($vaultName, ?string $description = NULL, ?Icon $icon = NULL, ?bool $allowAdminsToManage = NULL) : Vault {
        $cmd = 'vault create ' . escapeshellarg($vaultName);

        if ($description !== NULL) {
            $cmd .= ' --description ' . escapeshellarg($description);
        }
        if ($icon !== NULL) {
            $cmd .= ' --icon ' . $icon->value;
        }
        if ($allowAdminsToManage !== NULL) {
            $cmd .= ' --allow-admins-to-manage ' . ($allowAdminsToManage ? 'true' : 'false');
        }
        $output = $this->_runOPCommand($cmd);
        if ($output['exitcode'] !== 0) {
            throw new \RuntimeException('Unable to create vault. received exit code: ' . $output['exitcode'] . '. stdErr: ' . $output['stderr']);
        }
        return Vault::FromStdClass(json_decode($output['stdout'], flags: JSON_THROW_ON_ERROR));
    }

    public function deleteVault(string|Vault $vault) : void {
        if ($vault instanceof Vault) {
            $vault = $vault->id ?: $vault->name;
        }
        $cmd = 'vault delete ' . escapeshellarg($vault);

        $output = $this->_runOPCommand($cmd);
        if ($output['exitcode'] !== 0) {
            throw new \RuntimeException('Unable to delete vault. received exit code: ' . $output['exitcode'] . '. stdErr: ' . $output['stderr']);
        }
    }

    public function getVault(string $vault) : Vault {
        $cmd = 'vault get ' . escapeshellarg($vault);

        $output = $this->_runOPCommand($cmd);

        if ($output['exitcode'] !== 0) {
            if (str_contains($output['stderr'], 'More than one vault matches')) {
                throw new Vault\MultipleVaultsException($output['stderr']);
            } elseif (str_contains($output['stderr'], 'isn\'t a vault in this account.')) {
                throw new Vault\NotFoundException($output['stderr']);
            } else {
                throw new \RuntimeException('Unable to retrieve vault. received exit code: ' . $output['exitcode'] . '. stdErr: ' . $output['stderr']);
            }

        }
        return Vault::FromStdClass(json_decode($output['stdout'], flags: JSON_THROW_ON_ERROR));
    }

    /**
     * @return \Generator<TemplateReference>
     * @throws \JsonException
     */
    public function listTemplates() : \Generator {
        $cmd = 'item template list';

        $output = $this->_runOPCommand($cmd);
        if ($output['exitcode'] !== 0) {
            throw new \RuntimeException('Unable to list templates. received exit code: ' . $output['exitcode'] . '. stdErr: ' . $output['stderr']);
        }
        $templateReferences = json_decode($output['stdout'], flags: JSON_THROW_ON_ERROR);

        foreach ($templateReferences as $reference) {
            yield new TemplateReference($reference->uuid, $reference->name);
        }
    }

    public function getTemplate(string $templateName) : AItem {
        $cmd = 'item template get ' . escapeshellarg($templateName);

        $output = $this->_runOPCommand($cmd);
        if ($output['exitcode'] !== 0) {
            throw new \RuntimeException('Unable get template [' . $templateName . ']. received exit code: ' . $output['exitcode'] . '. stdErr: ' . $output['stderr']);
        }
        $data = json_decode($output['stdout'], flags: JSON_THROW_ON_ERROR);

        return ItemFactory::GetInstance()->createItem($data);
    }

    /**
     * @param array $tags
     * @return \Generator<AItem>
     * @throws \JsonException
     */
    public function listItems(array $tags = []) : \Generator {
        $cmd = 'item list';
        if (!empty($tags)) {
            $cmd .= ' --tags=' . escapeshellarg(implode(',', $tags));
        }

        $output = $this->_runOPCommand($cmd);
        if ($output['exitcode'] !== 0) {
            throw new \RuntimeException('Unable to list items. Received exit code: ' . $output['exitcode'] . '. stdErr: ' . $output['stderr']);
        }
        $data = json_decode($output['stdout'], flags: JSON_THROW_ON_ERROR);

        foreach ($data as $item) {
            yield ItemFactory::GetInstance()->createItem($item);
        }
    }

    public function getItem(string $item) : AItem {
        $output = $this->_runOPCommand('item get ' . escapeshellarg($item));
        if ($output['exitcode'] !== 0) {
            throw new \RuntimeException('Unable to retrieve item: ' . $item . ' received exit code: ' . $output['exitcode'] . '. stdErr: ' . $output['stderr']);
        }
        $data = json_decode($output['stdout'], flags: JSON_THROW_ON_ERROR);

        return ItemFactory::GetInstance()->createItem($data);
    }

    public function deleteItem(string|AItem $item) : void {
        if ($item instanceof AItem) {
            $item = $item->id ?: $item->title;
        }
        $cmd = 'item delete ' . escapeshellarg($item);

        $output = $this->_runOPCommand($cmd);
        if ($output['exitcode'] !== 0) {
            throw new \RuntimeException('Unable to delete Item. received exit code: ' . $output['exitcode'] . '. stdErr: ' . $output['stderr']);
        }
    }

    public function createItem(ItemBuilder $itemBuilder) : AItem {
        $cmd = 'cat - | op item create ';

        if ($itemBuilder->url !== NULL) {
            $cmd .= '--url=' . escapeshellarg($itemBuilder->url);
        }

        if ($itemBuilder->username !== NULL) {
            $cmd .= ' username=' . escapeshellarg($itemBuilder->username);
        }

        if ($itemBuilder->password !== NULL) {
            $cmd .= ' password=' . escapeshellarg($itemBuilder->password);
        }

        if (!empty($itemBuilder->getTags())) {
            $cmd .= ' --tags=' . implode(',', $itemBuilder->getTags());
        }

        $jsonStr = json_encode($itemBuilder, JSON_THROW_ON_ERROR);
        $output = $this->_runCommand($cmd, $jsonStr);

        if ($output['exitcode'] !== 0) {
            throw new \RuntimeException('Unable to create item! Received exit code: ' . $output['exitcode'] . '. stdErr: ' . $output['stderr']);
        }
        $data = json_decode($output['stdout'], FALSE, 512, JSON_THROW_ON_ERROR);

        $itemFactory = ItemFactory::GetInstance();
        return $itemFactory->createItem($data);
    }

    public function getReference(string $reference) : string {
        $output = $this->_runOPCommand('read ' . escapeshellarg($reference));
        if ($output['exitcode'] !== 0) {
            throw new \RuntimeException('Unable to retrieve reference: ' . $reference . ' received exit code: ' . $output['exitcode'] . '. stdErr: ' . $output['stderr']);
        }
        return $output['stdout'];
    }

    protected function _runOPCommand(string $command, ?string $stdin = NULL) : array {
        return $this->_runCommand('op ' . $command, $stdin);
    }

    protected function _runCommand(string $command, ?string $stdin = NULL) : array {
        return $this->_shell->run(($this->_session ? $this->_session . ' ': '') . $command, FALSE, $stdin ?: '');
    }

    public function isBinaryInstalled() : bool {
        return $this->_shell->runForExitCode('which op');
    }

    public function isBiometricsEnabled() : bool {
        return self::$_BiometricEnabled;
    }

    public function getInstalledVersion() : string {
        return trim($this->_shell->runForStdOut('op --version'));
    }

    public function isCorrectVersionInstalled() : bool {
        $opVersion = $this->getInstalledVersion();

        $versionParser = new VersionParser;
        $constraint = $versionParser->parseConstraints(self::CLI_VERSION_CONSTRAINT);

        return $constraint->matches($versionParser->parseConstraints($opVersion));
    }

    public function isSignedIn(?string $account = NULL) : bool {
        $whoAmI = $this->whoAmI();
        if ($whoAmI === NULL) {
            $this->_session = NULL;
            return FALSE;
        } elseif ($account) {
            $accountObj = $this->getAccount($account);
            if ($accountObj && $accountObj->url === $whoAmI->url) {
                return TRUE;
            } else {
                $this->_session = NULL;
                return FALSE;
            }
        }
        return TRUE;
    }

    public function whoAmI() : ?Account {
        $result = $this->_runOPCommand('whoami');
        if ($result['exitcode'] === 0) {
            return new Account(json_decode($result['stdout'], flags: JSON_THROW_ON_ERROR));
        }
        return NULL;
    }

    public function setMaxPasswordAttempts(int $attempts) : void {
        if ($attempts < 1) {
            throw new \UnexpectedValueException('Value for attempts cannot be lower than 1!');
        }
        $this->_maxPasswordAttempts = $attempts;
    }

    public static function SetUserInput(IUserInput $userInput) : void {
        self::$_UserInput = $userInput;
    }

    public static function SetBiometricEnabled(bool $enabled) : void {
        self::$_BiometricEnabled = $enabled;
    }

    private function _getCachedSessionForUserUUID(string $uuid) : ?string {
        $path = self::_GetCacheFilePath($uuid);
        if (is_file($path)) {
            return file_get_contents($path);
        }
        return NULL;
    }

    private function _removeCachedSessionForUserUUID(string $uuid) : void {
        $path = self::_GetCacheFilePath($uuid);
        if (is_file($path)) {
            unlink($path);
        }
    }

    private function _cacheSessionForUserUUID(string $uuid, string $session) : void {
        $path = self::_GetCacheFilePath($uuid);
        file_put_contents($path, $session);
    }

    private static function _GetCacheDir() : string {
        $cacheDir = $_SERVER['HOME'] . '/.cache/';
        if (!is_dir($cacheDir)) {
            mkdir($cacheDir, 0755, TRUE);
        }

        $baseDir = $cacheDir . 'bf-1pw-cli/';
        if (!is_dir($baseDir)) {
            mkdir($baseDir, 0700);
        }
        return $baseDir;
    }

    private static function _GetCacheFilePath(string $category) : string {
        return self::_GetCacheDir() . $category;
    }

    public static function ClearSessionCache() : void {
        $cacheDir = self::_GetCacheDir();
        foreach (new \RecursiveDirectoryIterator($cacheDir, \FilesystemIterator::SKIP_DOTS) as $fileInfo) {
            if ($fileInfo->isFile()) {
                unlink($fileInfo->getRealPath());
            }
        }
    }
}

putenv('OP_FORMAT=json');
