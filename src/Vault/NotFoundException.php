<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword\Vault;

class NotFoundException extends \RuntimeException {

}
