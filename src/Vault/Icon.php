<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword\Vault;

enum Icon : string {
    case AIRPLANE = 'airplane';
    case APPLICATION = 'application';
    case ART_SUPPLIES = 'art-supplies';
    case BANKERS_BOX = 'bankers-box';
    case BROWN_BRIEFCASE = 'brown-briefcase';
    case BROWN_GATE = 'brown-gate';
    case BUILDINGS = 'buildings';
    case CABIN = 'cabin';
    case CASTLE = 'castle';
    case CIRCLE_OF_DOTS = 'circle-of-dots';
    case COFFEE = 'coffee';
    case COLOR_WHEEL = 'color-wheel';
    case CURTAINED_WINDOW = 'curtained-window';
    case DOCUMENT = 'document';
    case DOUGHNUT = 'doughnut';
    case FENCE = 'fence';
    case GALAXY = 'galaxy';
    case GEARS = 'gears';
    case GLOBE = 'globe';
    case GREEN_BACKPACK = 'green-backpack';
    case GREEN_GEM = 'green-gem';
    case HANDSHAKE = 'handshake';
    case HEART_WITH_MONITOR = 'heart-with-monitor';
    case HOUSE = 'house';
    case ID_CARD = 'id-card';
    case JET = 'jet';
    case LARGE_SHIP = 'large-ship';
    case LUGGAGE = 'luggage';
    case PLANT = 'plant';
    case PORTHOLE = 'porthole';
    case PUZZLE = 'puzzle';
    case RAINBOW = 'rainbow';
    case RECORD = 'record';
    case ROUND_DOOR = 'round-door';
    case SANDALS = 'sandals';
    case SCALES = 'scales';
    case SCREWDRIVER = 'screwdriver';
    case SHOP = 'shop';
    case TALL_WINDOW = 'tall-window';
    case TREASURE_CHEST = 'treasure-chest';
    case VAULT_DOOR = 'vault-door';
    case VEHICLE = 'vehicle';
    case WALLET = 'wallet';
    case WRENCH = 'wrench';
}
