<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword;

class Account {

    private const _DOMAIN_REGEX = '/^(?>https:\/\/)?(?<domain>(?<shorthand>.+)\.1password\.com)\/?$/';

    public readonly string $url;
    public readonly string $shorthand;
    public readonly string $email;
    public readonly ?string $userUuid;
    public readonly ?string $accountUuid;

    public ?string $secretKey = NULL;

    private ?string $_domainName = NULL;

    public function __construct(\stdClass $data) {
        $this->url = $data->url;
        $shorthand = $data->shorthand ?? NULL;
        if ($shorthand === NULL) {
            if (!preg_match(self::_DOMAIN_REGEX, $this->url, $matches)) {
                throw new \LogicException('Unable to determine shorthand for url: ' . $this->url);
            }
            $shorthand = $matches['shorthand'];
        }
        $this->shorthand = $shorthand;

        $this->email = $data->email;
        $this->userUuid = $data->user_uuid ?? NULL;
        $this->accountUuid = $data->account_uuid ?? NULL;
    }

    public function getDomainName() : string {
        if ($this->_domainName === NULL) {
            $this->_domainName = static::NormalizeDomainName($this->url);
        }
        return $this->_domainName;
    }

    public static function NormalizeDomainName(string $domainName) : string {
        return preg_replace(self::_DOMAIN_REGEX, '$1', $domainName);
    }

    public static function IsValidDomain(string $domain) : bool {
        return (bool) preg_match(self::_DOMAIN_REGEX, $domain);
    }
}
