<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword\SymfonyConsole;

use Brightfish\OnePassword\Account;
use Brightfish\OnePassword\IUserInput;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

if (!interface_exists(InputInterface::class)) {
    throw new \RuntimeException('Unable to make use of ' . UserInput::class . ' package symphony/console is not installed! Please run \'composer install symfony/console\'');
}

class UserInput implements IUserInput {

    private readonly QuestionHelper $_questionHelper;

    public function __construct(private readonly InputInterface $_input, private readonly OutputInterface $_output) {
        $this->_questionHelper = new QuestionHelper;
    }

    public function getPasswordForAccount(string $email, string $url, int $attemptNumber) : string {
        if ($attemptNumber > 0) {
            $this->_output->writeln('<error>Entered invalid password for ' . $email . ' at ' . $url . '</error>');
        }
        $question = new Question('Enter the password for ' . $email . ' at ' . $url . ': ');
        $question->setHidden(TRUE);

        return $this->_questionHelper->ask($this->_input, $this->_output, $question);
    }


    public function addAccount(?string $signInAddress, ?string $email) : Account {
        $data = new \stdClass;

        //Enter your sign-in address
        if ($signInAddress === NULL) {
            $question = new Question('Enter your sign-in address (example.1password.com): ');
            $question->setValidator(function (string $value) : string {
                if (!Account::IsValidDomain($value)) {
                    throw new \RuntimeException($value . ' is not a valid 1password domain!');
                }
                return $value;
            });
            $question->setNormalizer(function (string $value) : string {
                return Account::NormalizeDomainName($value);
            });
            $result = $this->_questionHelper->ask($this->_input, $this->_output, $question);
            $data->url = $result;
        } else {
            $data->url = $signInAddress;
        }

        if ($email === NULL) {
            $question = new Question('Enter the email address for your account on ' . $data->url . ': ');
            $question->setValidator(function (string $value) : string {
                if (filter_var($value, FILTER_VALIDATE_EMAIL) === FALSE) {
                    throw new \RuntimeException($value . ' is not a valid email address!');
                }
                return $value;
            });
            $data->email = $this->_questionHelper->ask($this->_input, $this->_output, $question);
        } else {
            $data->email = $email;
        }

        $secretKeyQuestion = new Question('Enter the Secret Key for ' . $data->email . ' on ' . $data->url . ': ');
        $secret = $this->_questionHelper->ask($this->_input, $this->_output, $secretKeyQuestion);

        $account = new Account($data);
        $account->secretKey = $secret;

        return $account;
    }
}
