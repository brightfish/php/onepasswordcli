<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword;

use Brightfish\OnePassword\Item\Category;
use Brightfish\OnePassword\Item\Field\Type;
use Brightfish\OnePassword\ItemBuilder\Exception;
use Brightfish\OnePassword\ItemBuilder\Field;

class ItemBuilder implements \JsonSerializable {

    public string $title;
    public Category $category;

    public ?string $url = NULL;
    public ?string $username = NULL;
    public ?string $password = NULL;

    /**
     * @var Section[]
     */
    private array $_sections = [];
    private ?Vault $_vault = NULL;

    /**
     * @var Field[]
     */
    private array $_fields = [];

    /**
     * @var string[]
     */
    private array $_tags = [];

    public function __construct(public readonly CLI $cli) {
    }

    public function setVault(?Vault $vault) : void {
        $this->_vault = $vault;
    }

    public function addField(Field $field) {
        if ($field->section !== NULL && !$this->hasSection($field->section->label)) {
            $this->addSection($field->section);
        }
        $this->_fields[] = $field;
    }

    public function createPasswordField(string $label = 'Password') : Field {
        $field = new Field;
        $field->type = Type::Concealed;
        $field->label = $label;

        return $field;
    }

    public function createTextField() : Field {
        $field = new Field;
        $field->type = Type::String;

        return $field;
    }

    public function createURLField() : Field {
        $field = new Field;
        $field->type = Type::URL;

        return $field;
    }

    /**
     * @return Field[]
     */
    public function getFields() : array {
        return $this->_fields;
    }

    public function getFieldByLabel(string $label) : ?Field {
        foreach ($this->_fields as $field) {
            if ($field->label === $label) {
                return $field;
            }
        }
        return NULL;
    }

    public function hasSection(string $sectionName) : bool {
        foreach ($this->_sections as $section) {
            if ($section->label === $sectionName) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function createSection(string $sectionName) : Section {
        return new Section(id: NULL, label: $sectionName);
    }

    public function addSectionWithName(string $sectionName) : void {
        $this->addSection($this->createSection($sectionName));
    }

    public function addSection(Section $section) : void {
        $this->_sections[] = $section;
    }

    public function jsonSerialize() : mixed {
        if (!isset($this->title)) {
            throw new Exception('Cannot serialize Item: Title has not been set!');
        }
        if (!isset($this->category)) {
            throw new Exception('Cannot serialize Item: Category has not been set!');
        }
        $stdClass = new \stdClass;
        $stdClass->title = $this->title;

        $stdClass->category = $this->category instanceof \UnitEnum ? $this->category->value : $this->category;
        $stdClass->sections = $this->_sections;
        if ($this->_vault) {
            $stdClass->vault = $this->_vault;
        }
        $stdClass->fields = $this->_fields;

        return $stdClass;
    }

    public function create() : AItem {
        if ($this->category === Category::Custom) {
            throw new Exception('Unable to create item. Category has been set to Custom which is not a validated type!');
        }
        return $this->cli->createItem($this);
    }

    public function addTag(string $tag) : void {
        $this->_tags[] = $tag;
    }

    public function getTags() : array {
        return $this->_tags;
    }

    public static function FromItem(CLI $cli, AItem $item) : static {
        $builder = new static($cli);
        if ($item->vault !== NULL) {
            $builder->setVault($item->vault);
        }

        $builder->category = $item->category;
        $builder->title = $item->title;
        $builder->_sections = $item->sections;

        foreach ($item->getFields() as $field) {
            $builder->addField(Field::FromReadonlyField($field));
        }

        return $builder;
    }
}
