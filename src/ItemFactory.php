<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword;

use Brightfish\OnePassword\Item\APICredential;
use Brightfish\OnePassword\Item\BankAccount;
use Brightfish\OnePassword\Item\Category;
use Brightfish\OnePassword\Item\CreditCard;
use Brightfish\OnePassword\Item\Custom;
use Brightfish\OnePassword\Item\Database;
use Brightfish\OnePassword\Item\Document;
use Brightfish\OnePassword\Item\EmailAccount;
use Brightfish\OnePassword\Item\SecureNote;
use Brightfish\OnePassword\Item\Server;
use Brightfish\OnePassword\Item\SocialSecurityNumber;
use Brightfish\OnePassword\Item\SoftwareLicense;
use Brightfish\OnePassword\Item\SSHKey;
use Brightfish\OnePassword\Item\Login;
use Brightfish\OnePassword\Item\Password;
use Brightfish\OnePassword\Item\WirelessRouter;

class ItemFactory {

    private static string $_FactoryClass = ItemFactory::class;

    private static ?ItemFactory $_Instance = NULL;

    private function __construct() {

    }

    /**
     * @param \stdClass $item
     * @return AItem|Login|Password|WirelessRouter
     */
    public function createItem(\stdClass $item) : AItem {
        $category = Category::tryFrom($item->category);

        return match ($category) {
            Category::Login => $this->createLoginItem($item),
            Category::Password => $this->createPasswordItem($item),
            Category::WirelessRouter => $this->createWirelessRouterItem($item),
            Category::Database => $this->createDatabaseItem($item),
            Category::Custom => $this->createCustomItem($item),
            Category::CreditCard => $this->createCreditCardItem($item),
            Category::SoftwareLicense => $this->createSoftwareLicenseItem($item),
            Category::Document => $this->createDocumentItem($item),
            Category::SecureNote => $this->createSecureNoteItem($item),
            Category::APICredential => $this->createAPICredentialItem($item),
            Category::Server => $this->createServerItem($item),
            Category::EmailAccount => $this->createEmailAccountItem($item),
            Category::SocialSecurityNumber => $this->createSocialSecurityNumberItem($item),
            Category::BankAccount => $this->createBankAccountItem($item),
            Category::SSHKey => $this->createSSHKeyItem($item),
            default => throw new \RuntimeException('Unsupported item category: ' . $item->category),
        };
    }

    public function createLoginItem(\stdClass $item) : Login {
        return new Login($item);
    }

    public function createCustomItem(\stdClass $custom) : Custom {
        return new Custom($custom);
    }

    public function createPasswordItem(\stdClass $item) : Password {
        return new Password($item);
    }

    public function createWirelessRouterItem(\stdClass $item) : WirelessRouter {
        return new WirelessRouter($item);
    }

    public function createDatabaseItem(\stdClass $item) : Database {
        return new Database($item);
    }

    public function createCreditCardItem(\stdClass $item) : CreditCard {
        return new CreditCard($item);
    }

    public function createSoftwareLicenseItem(\stdClass $item) : SoftwareLicense {
        return new SoftwareLicense($item);
    }

    public function createDocumentItem(\stdClass $item) : Document {
        return new Document($item);
    }

    public function createSecureNoteItem(\stdClass $item) : SecureNote {
        return new SecureNote($item);
    }

    public function createAPICredentialItem(\stdClass $item) : APICredential {
        return new APICredential($item);
    }

    public function createServerItem(\stdClass $item) : Server {
        return new Server($item);
    }

    public function createEmailAccountItem(\stdClass $item) : EmailAccount {
        return new EmailAccount($item);
    }

    public function createSocialSecurityNumberItem(\stdClass $item) : SocialSecurityNumber {
        return new SocialSecurityNumber($item);
    }

    public function createBankAccountItem(\stdClass $item) : BankAccount {
        return new BankAccount($item);
    }

    public function createSSHKeyItem(\stdClass $item) : SSHKey {
        return new SSHKey($item);
    }

    public static function SetFactoryClass(string $class) : void {
        if (!is_a($class, ItemFactory::class, TRUE)) {
            throw new \LogicException('Class ' . $class . ' does not extend ' . ItemFactory::class);
        }
        self::$_FactoryClass = $class;
    }

    public static function GetInstance() : static {
        if (self::$_Instance === NULL) {
            self::$_Instance = new self::$_FactoryClass;
        }
        return self::$_Instance;
    }
}
