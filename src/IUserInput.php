<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword;

interface IUserInput {
    public function getPasswordForAccount(string $email, string $url, int $attemptNumber) : string;

    public function addAccount(?string $signInAddress, ?string $email) : Account;
}
