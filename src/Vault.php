<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword;

class Vault {

    public function __construct(public readonly ?string $id, public readonly string $name, public readonly  ?int $contentVersion = NULL) {
    }

    public static function FromStdClass(\stdClass $stdClass) : static {
        return new static($stdClass->id ?? NULL, $stdClass->name, $stdClass->content_version ?? $stdClass->contentVersion ?? NULL);
    }
}
