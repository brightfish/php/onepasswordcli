<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword;

class TemplateReference {

    public function __construct(public readonly string $uuid, public readonly string $name) {

    }

}
