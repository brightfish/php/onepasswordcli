<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword;

class Section {

    public function __construct(public readonly ?string $id, public readonly ?string $label) {

    }

}
