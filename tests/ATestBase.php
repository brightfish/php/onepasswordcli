<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword\Tests;

use Brightfish\OnePassword\AItem;
use Brightfish\OnePassword\CLI;
use Brightfish\OnePassword\ItemFactory;
use PHPUnit\Framework\TestCase;

abstract class ATestBase extends TestCase {

    private ?CLI $_cli = NULL;

    /**
     * @var \stdClass[]
     */
    private array $_items;

    protected function setUp() : void {
        if ($this->_getEnvVar('1PW_ACCOUNT') === NULL) {
            $this->markTestSkipped('Unable to run test, 1PW account env var not set!');
        }
    }


    public function getCLI() : CLI {
        if ($this->_cli === NULL) {
            $this->_cli = new CLI;
        }
        $this->_cli->signIn($this->getAccount());

        return $this->_cli;
    }

    public function getAccount() : string {
        $account = $this->_getEnvVar('1PW_ACCOUNT');
        $this->assertIsString($account);
        return $account;
    }

    protected function _getEnvVar(string $key) : ?string {
        return $_ENV[$key] ?? NULL;
    }

    public function getJSONItems() : array {
        $this->assertDirectoryExists(__DIR__ . '/Items');
        if (!isset($this->_items)) {
            $this->_items = [];
            foreach (new \DirectoryIterator(__DIR__ . '/Items') as $item) {
                if ($item->isFile() && $item->getExtension() === 'json') {
                    $this->_items[] = $item = json_decode(file_get_contents($item->getRealPath()), flags: JSON_THROW_ON_ERROR);
                    $this->assertInstanceOf(\stdClass::class, $item);
                }
            }
        }

        return $this->_items;
    }

    public function getItem(string $type) : AItem {
        $this->assertDirectoryExists(__DIR__ . '/Items');
        $this->assertFileExists(__DIR__ . '/Items/' . strtolower($type) . '.json');

        $contents = json_decode(file_get_contents(__DIR__ . '/Items/' . strtolower($type) . '.json'));
        $itemFactory = ItemFactory::GetInstance();
        return $itemFactory->createItem($contents);
    }

    public function testBinaryInstalled() : void {
        $cli = $this->getCLI();
        $this->assertInstanceOf(CLI::class, $cli);
        $this->assertTrue($cli->isBinaryInstalled());
    }
}
