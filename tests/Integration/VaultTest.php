<?php

declare(strict_types = 1);

namespace Brightfish\Onepassword\Tests\Integration;

use Brightfish\OnePassword\Tests\ATestBase;
use Brightfish\OnePassword\Vault;

class VaultTest extends ATestBase {

    private const _VAULT_NAME = 'af08aeraf089lafalkj';

    public function testCreateVault() : Vault {
        $vault = $this->getCLI()->createVault(self::_VAULT_NAME);
        $this->_assertVault($vault);

        return $vault;
    }

    /**
     * @depends testCreateVault
     */
    public function testCreateSameVault(Vault $vault) : array {
        $vault2 = $this->getCLI()->createVault(self::_VAULT_NAME);
        $this->_assertVault($vault2);

        return [$vault, $vault2];
    }

    /**
     * @depends testCreateSameVault
     * @param Vault[] $vaults
     */
    public function testGetVaultDuplicateVaultException(array $vaults) : array {
        $this->expectException(Vault\MultipleVaultsException::class);
        $vault = reset($vaults);

        $this->getCLI()->getVault($vault->name);

        return $vaults;
    }

    /**
     * @depends testGetVaultDuplicateVaultException
     */
    public function testListAndDeleteVaults() : void {
        $cli = $this->getCLI();
        foreach ($cli->listVaults() as $vault) {
            if ($vault->name === self::_VAULT_NAME) {
                $cli->deleteVault($vault);
            }
        }
    }

    /**
     * @depends testListAndDeleteVaults
     */
    public function testGetVaultNotFoundException() : void {
        $this->expectException(Vault\NotFoundException::class);
        $this->getCLI()->getVault(self::_VAULT_NAME);
    }

    private function _assertVault($vault) : void {
        $this->assertInstanceOf(Vault::class, $vault);
        $this->assertEquals(self::_VAULT_NAME, $vault->name);
    }
}
