<?php

declare(strict_types = 1);

namespace Brightfish\OnePassword\Tests\Integration;

use Brightfish\OnePassword\AItem;
use Brightfish\OnePassword\Item\Category;
use Brightfish\OnePassword\Item\Custom;
use Brightfish\OnePassword\Item\Password;
use Brightfish\OnePassword\ItemBuilder;
use Brightfish\OnePassword\Tests\ATestBase;

class ItemTest extends ATestBase {

    public function testListItems() : void {
        $cli = $this->getCLI();
        foreach ($cli->listItems() as $item) {
            $this->assertInstanceOf(AItem::class, $item);
        }
    }

    public function testCreateItemBuilder() : ItemBuilder {
        $builder = new ItemBuilder($this->getCLI());
        $this->assertInstanceOf(ItemBuilder::class, $builder);

        return $builder;
    }

    /**
     * @depends testCreateItemBuilder
     */
    public function testCreateItemFromScratch(ItemBuilder $itemBuilder) : AItem {
        $itemBuilder->title = 'Test item';
        $itemBuilder->category = Category::Password;

        $itemBuilder->password = 'SomePassword';

        $passwordField = $itemBuilder->createPasswordField();
        $passwordField->value = 'This is a very difficult password';

        $passwordField->section = $itemBuilder->createSection('Password');

        $itemBuilder->addField($passwordField);

        $item = $itemBuilder->create();
        $this->assertInstanceOf(Password::class, $item);

        return $item;
    }

    /**
     * @depends testCreateItemFromScratch
     */
    public function testDeleteItem(AItem $item) : void {
        $this->getCLI()->deleteItem($item);
    }

    public function testCreateItemBuilderFromExistingItem() : ItemBuilder {
        $item = $this->getItem('custom');
        $this->assertInstanceOf(Custom::class, $item);

        $builder = ItemBuilder::FromItem($this->getCLI(), $item);
        $this->assertInstanceOf(ItemBuilder::class, $builder);

        return $builder;
    }

    public function testCreateFromCustomException() : void {
        $this->expectException(ItemBuilder\Exception::class);
        $item = $this->getItem('custom');
        $this->assertInstanceOf(Custom::class, $item);

        $builder = ItemBuilder::FromItem($this->getCLI(), $item);
        $this->assertInstanceOf(ItemBuilder::class, $builder);

        $builder->create();
    }

    /**
     * @depends testCreateItemBuilderFromExistingItem
     */
    public function testCreateItemFromExistingItem(ItemBuilder $itemBuilder) : void {
        $itemBuilder->category = Category::Password;
        $itemBuilder->title = 'Test stuffszzzz';
        $itemBuilder->password = 'some password';

        $item = $itemBuilder->create();
        $this->assertInstanceOf(Password::class, $item);

        $this->getCLI()->deleteItem($item);
    }

}
