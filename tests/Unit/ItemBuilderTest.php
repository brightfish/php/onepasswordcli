<?php

declare(strict_types = 1);

use Brightfish\OnePassword\Item\Category;
use Brightfish\OnePassword\Item\Field\Type;
use Brightfish\OnePassword\ItemBuilder;
use Brightfish\OnePassword\Tests\ATestBase;

class ItemBuilderTest extends ATestBase {

    public function testJsonSerializeException() : void {
        $this->expectException(ItemBuilder\Exception::class);

        $itemBuilder = $this->_getItemBuilder();

        json_encode($itemBuilder);
    }

    public function testJsonSerialize() : void {
        $itemBuilder = $this->_getItemBuilder();

        $itemBuilder->title = 'Some secure item';
        $itemBuilder->category = Category::Password;

        $password = $itemBuilder->createSection('Password');

        $field = $itemBuilder->createPasswordField();
        $field->value = 'Some secure password';
        $field->section = $password;

        $itemBuilder->addField($field);

        $json = json_encode($itemBuilder);
        $this->assertEquals('{"title":"Some secure item","category":"PASSWORD","sections":[{"id":null,"label":"Password"}],"fields":[{"label":"Password","type":"CONCEALED","section":{"id":null,"label":"Password"},"value":"Some secure password"}]}', $json);

    }

    public function testCreateField() : void {
        $itemBuilder = $this->_getItemBuilder();

        $password = $itemBuilder->createPasswordField();
        $this->assertInstanceOf(ItemBuilder\Field::class, $password);
        $this->assertEquals(Type::Concealed, $password->type);

        $textField = $itemBuilder->createTextField();
        $this->assertInstanceOf(ItemBuilder\Field::class, $textField);
        $this->assertEquals(Type::String, $textField->type);

        $urlField = $itemBuilder->createURLField();
        $this->assertInstanceOf(ItemBuilder\Field::class, $urlField);
        $this->assertEquals(Type::URL, $urlField->type);
    }


    protected function _getItemBuilder() : ItemBuilder {
        return new ItemBuilder($this->getCLI());
    }

}
