<?php

declare(strict_types = 1);

namespace Brightfish\Onepassword\Tests\Unit;

use Brightfish\OnePassword\AItem;
use Brightfish\OnePassword\ItemFactory;
use Brightfish\OnePassword\Tests\ATestBase;

class ItemGeneratorTest extends ATestBase {

    public function testCreateItems() : void {
        $itemFactory = ItemFactory::GetInstance();
        $this->assertInstanceOf(ItemFactory::class, $itemFactory);

        foreach ($this->getJSONItems() as $item) {
            $itemObj = $itemFactory->createItem($item);
            $this->assertInstanceOf(AItem::class, $itemObj);
        }
    }

}
